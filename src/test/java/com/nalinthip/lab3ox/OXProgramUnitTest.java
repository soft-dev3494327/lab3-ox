/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.nalinthip.lab3ox;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ACER
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBY_O_output_false() {
        String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
        String player = "O";
        assertEquals(false, OXProgram.checkWin(board, player));

    }

    //Row O
    @Test
    public void testCheckWinRow1PlayBY_O_output_true() {
        String[][] board = {{"O", "O", "O"}, {"_", "_", "_"}, {"_", "X", "X"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
        
    }
    
    @Test
    public void testCheckWinRow2PlayBY_O_output_true() {
        String[][] board = {{"X", "_", "_"}, {"O", "O", "O"}, {"X", "_", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));

    }
    
    @Test
    public void testCheckWinRow3PlayBY_O_output_true() {
        String[][] board = {{"_", "X", "X"}, {"_", "_", "_"}, {"O", "O", "O"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));

    }
    
    //Row X
    @Test
    public void testCheckWinRow1PlayBY_X_output_true() {
        String[][] board = {{"X", "X", "X"}, {"_", "_", "_"}, {"_", "O", "O"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));

    }
    
    @Test
    public void testCheckWinRow2PlayBY_X_output_true() {
        String[][] board = {{"O", "_", "_"}, {"X", "X", "X"}, {"O", "_", "_"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));

    }
    
    @Test
    public void testCheckWinRow3PlayBY_X_output_true() {
        String[][] board = {{"_", "O", "O"}, {"_", "_", "_"}, {"X", "X", "X"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));

    }
    
    //Col O
    @Test
    public void testCheckWinCol1PlayBY_O_output_true() {
        String[][] board = {{"O", "X", "_"}, {"O", "_", "_"}, {"O", "X", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    @Test
    public void testCheckWinCol2PlayBY_O_output_true() {
        String[][] board = {{"X", "O", "_"}, {"X", "O", "_"}, {"_", "O", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    @Test
    public void testCheckWinCol3PlayBY_O_output_true() {
        String[][] board = {{"X", "_", "O"}, {"X", "_", "O"}, {"_", "_", "O"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    //Col X
    @Test
    public void testCheckWinCol1PlayBY_X_output_true() {
        String[][] board = {{"X", "O", "_"}, {"X", "_", "_"}, {"X", "O", "_"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    @Test
    public void testCheckWinCol2PlayBY_X_output_true() {
        String[][] board = {{"O", "X", "_"}, {"_", "X", "_"}, {"O", "X", "_"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    @Test
    public void testCheckWinCol3PlayBY_X_output_true() {
        String[][] board = {{"O", "_", "X"}, {"_", "_", "X"}, {"O", "_", "X"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    //X1 O
    @Test
    public void testCheckWinX1PlayBY_O_output_true() {
        String[][] board = {{"O", "_", "X"}, {"_", "O", "X"}, {"_", "_", "O"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    //X2 O
    @Test
    public void testCheckWinX2PlayBY_O_output_true() {
        String[][] board = {{"X", "_", "O"}, {"_", "O", "X"}, {"O", "_", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    //X1 X
    @Test
    public void testCheckWinX1PlayBY_X_output_true() {
        String[][] board = {{"X", "_", "O"}, {"_", "X", "O"}, {"_", "_", "X"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    //X2 X
    @Test
    public void testCheckWinX2PlayBY_X_output_true() {
        String[][] board = {{"O", "_", "X"}, {"_", "X", "O"}, {"X", "_", "_"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    
    //Draw
    @Test
    public void testCheckDraw_output_true() {
        String[][] board = {{"O", "X", "O"}, {"O", "X", "O"}, {"X", "O", "X"}};
        String player = "X";
        assertEquals(true, OXProgram.checkDraw(board, player));
    }
}
