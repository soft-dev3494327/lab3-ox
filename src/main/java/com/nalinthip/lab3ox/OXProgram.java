/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nalinthip.lab3ox;

/**
 *
 * @author ACER
 */
class OXProgram {

    //Refactor
    static boolean checkWin(String[][] board, String player) {
        if (checkRow(board, player) || checkCol(board, player) || checkX1(board, player) || checkX2(board, player)) {
            return true;
        }

        return false;
    }

    static boolean checkDraw(String[][] board, String player) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ("_".equals(board[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkCol(String[][] board, String player) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(board, player, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] board, String player) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(board, player, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] board, String player, int row) {
        return board[row][0].equals(player) && board[row][1].equals(player) && board[row][2].equals(player);
    }

    private static boolean checkCol(String[][] board, String player, int col) {
        return board[0][col].equals(player) && board[1][col].equals(player) && board[2][col].equals(player);
    }

    private static boolean checkX1(String[][] board, String player) {
        return board[0][0].equals(player) && board[1][1].equals(player) && board[2][2].equals(player);
    }

    private static boolean checkX2(String[][] board, String player) {
        return board[0][2].equals(player) && board[1][1].equals(player) && board[2][0].equals(player);
    }

}
